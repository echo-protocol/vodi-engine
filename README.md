# VoDI Engine

The Vote Deployment and Implementation (VoDI) Engine lies at the heart of the Echo Protocol and enables the deployment of dedicated Ballot-Box Smart Contracts.

Ballot-Box Smart Contracts implement secure and transparent collection, verification, aggregation and tally of ballots for a specific vote event, as well as publication of the vote's results.
